@extends('layouts.app')

@section('content')
<div class="container">
    <img src="{{asset('images/'.Auth::user()->photo)}}" class="rounded float-left" alt="profile image"  width="200">
</br></br></br>
    <table class="table">
        <tbody>
            <tr>
              <th scope="row">first name</th>
              <td>{{Auth::user()->first_name}}</td>
            </tr>
            <tr>
                <th scope="row">last name</th>
                <td>{{Auth::user()->last_name}}</td>
              </tr>
              <tr>
                <th scope="row">email </th>
                <td>{{Auth::user()->email}}</td>
              </tr>
              <tr>
                <th scope="row">gender</th>
                <td>{{Auth::user()->gender}}</td>
              </tr>
              <tr>
                <th scope="row">date of birth </th>
                <td>{{Auth::user()->date_of_birth}}</td>
              </tr>
              <tr>
                <th scope="row">leave balance</th>
                @if (Auth::user()->leave_balance>1)
                <td>{{Auth::user()->leave_balance ." days"}}</td>
                @else
                <td>{{Auth::user()->leave_balance ." day"}}</td>
                @endif
              </tr>
              <tr>
                <th scope="row">position </th>
                <td>{{Auth::user()->position}}</td>
              </tr>
        </tbody>

    </table>
</div>
@endsection
