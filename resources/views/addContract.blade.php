@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('add user') }}</div>


                <div class="card-body">
                    <form method="POST" action="{{ route('insertContract') }}">
                        @csrf
                        @if (isset($alert))
                        <div class="alert alert-success" role="alert">
                           contract added
                          </div>
                        @endif
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('employee') }}</label>

                        <select class="form-select" aria-label="Default select example" name="employee">
                            @foreach ($users as $user)
                            <option value="{{$user->id}}">{{$user->first_name." ".$user->last_name}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('beginning') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="date" class="form-control " name="beginning" value="" required >
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('type of contract') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control " name="type" value="" required >
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('salary') }}</label>
                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control " name="salary" value="" required >
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('add') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
