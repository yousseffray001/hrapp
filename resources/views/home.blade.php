@extends('layouts.app')

@section('content')
<?php $j=0 ;?>
@foreach ($users as $user)
@if(Auth::user()->id==$user->id)
@break
@endif
<?php $j++ ;?>
@endforeach
<?php
$myAllRequests=$all[$j];
$myAcceptedRequests=$accepted[$j];
$myRefusedRequests=$refused[$j];
$myPendingRequests=$myAllRequests-($myAcceptedRequests+$myRefusedRequests);


$indexOfMax = array_search(max($all), $all);
$indexOfMin = array_search(min($all), $all);
$MaxName= "'".$users[$indexOfMax]->first_name ." ".$users[$indexOfMax]->last_name ."'"  ;
$MinName="'".$users[$indexOfMin]->first_name ." ".$users[$indexOfMin]->last_name ."'" ;
$MaxMinNames = $MaxName.','.$MinName ;



$allUsers="";
foreach ($users as $user) {
$allUsers= "'".$allUsers.$user->first_name." ".$user->last_name."','" ;
}
$allUsers=substr($allUsers, 1, -2);
//allrequests
$allrequests="";

foreach ($all as $v1) {
$allrequests= "'".$allrequests.$v1."','" ;
}
$allrequests=substr($allrequests, 1, -2);
//accepted
$acceptedrequests="";
foreach ($accepted as $v2) {
$acceptedrequests= "'".$acceptedrequests.$v2."','" ;
}
$acceptedrequests=substr($acceptedrequests, 1, -2);
//refused
$refusedrequests="";
foreach ($refused as $v3) {
$refusedrequests= "'".$refusedrequests.$v3."','" ;
}
$refusedrequests=substr($refusedrequests, 1, -2);
?>

<div class="container">
<div
@if(Auth::user()->role=='user')
style="display: none;"
@endif
>
        <h1 class="text-primary">all leave requests</h1>
            <div>
                <canvas id="myChart"  height="100"  ></canvas> </br></br></br>
            </div>
        <h1  class="text-primary">max and min leave requests</h1>
            <div>
                <canvas id="mybarChart"   height="100"></canvas> </br></br></br>
              </div>
 </div>


    <h1  class="text-primary">my leave requests</h1>
    <div>
      <canvas id="mybarChart2"  height="100" ></canvas> </br></br></br>
    </div>
    </div>



<script>
    //all leave requests
    const ctx = document.getElementById('myChart').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
                <?php
                     echo $allUsers ;
                ?>

        ],
            datasets: [{
                label: 'leave requests',
                data: [
                    <?php
                        echo $allrequests ;
                    ?>

                ],
                borderColor: [
                    'rgba(0, 0, 196, 1)'

                ],
                borderWidth: 2,
                 tension: 0.2
            },
            //
            {
                label: 'accepted',
                data: [
                    <?php
                        echo $acceptedrequests ;
                    ?>

                ],
                borderColor: [
                    'rgba(0, 206, 27, 1)'
                ],
                borderWidth: 3,
                 tension: 0.2
            },
  //
            {
                label: 'refused',
                data: [
                    <?php
                        echo $refusedrequests ;
                    ?>
                ],
                borderColor: [
                    'rgba(252, 17, 17, 1)'
                ],
                borderWidth: 3,
                 tension: 0.2
            },
            ]

        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    //bar chart max min
    var ctx1 = document.getElementById("mybarChart").getContext("2d");

  var mybarChart = new Chart(ctx1, {
    type: 'bar',
    data: {
      labels: [
        <?php
            echo $MaxMinNames ;
         ?>
      ],

      datasets: [{
        label: 'MAX',

        backgroundColor: " #ff0000",
        data: [
            <?php
                 echo max($all) ;
             ?>

        ,0]
      }, {
        label: 'MIN',
        backgroundColor: "#00FF00",
        data: [0,
        <?php
                 echo min($all) ;
             ?>
    ]
      },
    ]
    },



  });
//my leave requests
var ctx2 = document.getElementById("mybarChart2").getContext("2d");

var mybarChart2 = new Chart(ctx2, {
  type: 'bar',
  data: {
    labels: ['all requests', 'accepted' ,'refused','pending'],

    datasets: [{
      label: 'all requests',

      backgroundColor: " #2316D4",
      data: [
        <?php
                 echo $myAllRequests ;
        ?>

      ,0,0,0]
    }, {
      label: 'accepted',
      backgroundColor: "#16D430",
      data: [0,
      <?php
                 echo $myAcceptedRequests ;
        ?>
      ,0,0]
    },
    {
      label: 'refused',
      backgroundColor: "#D41616",
      data: [0,0,
      <?php
                 echo $myRefusedRequests ;
        ?>
      ,0]
    },
    {
      label: 'pending',
      backgroundColor: "#554949",
      data: [0,0,0,
      <?php
                 echo $myPendingRequests ;
        ?>
    ]
    },
  ]
  },



});



















    </script>









@endsection
