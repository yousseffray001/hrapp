@extends('layouts.app')
@section('content')
<div class="container">
@if (count($leaves)==0)
<div class="alert alert-info" role="alert">
  You do not have any leave request
</div>
@else

<table class="table">
    <thead>
      <tr>
        <th scope="col">To</th>
        <th scope="col">Beginnig</th>
        <th scope="col">End</th>
        <th scope="col">Request Status</th>
      </tr>
    </thead>

        @foreach ($leaves as $leave)
        @if ($leave->request_status=='pending')
        <tr class="table-secondary">
            <td>{{$leave->first_name." ".$leave->last_name}}</td>
            <td>{{$leave->beginning}}</td>
            <td>{{$leave->end}}</td>
            <td>{{$leave->request_status}}</td>
        </tr>
        @elseif($leave->request_status=='accepted')
        <tr class="table-success">
            <td>{{$leave->first_name." ".$leave->last_name}}</td>
            <td>{{$leave->beginning}}</td>
            <td>{{$leave->end}}</td>
            <td>{{$leave->request_status}}</td>
        </tr>
        @else
        <tr class="table-danger">
            <td>{{$leave->first_name." ".$leave->last_name}}</td>
            <td>{{$leave->beginning}}</td>
            <td>{{$leave->end}}</td>
            <td>{{$leave->request_status}}</td>
        </tr>
        @endif
        @endforeach

</table>
</div>
@endif
@endsection

