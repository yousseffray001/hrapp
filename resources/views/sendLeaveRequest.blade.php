@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Send Leave Request') }}</div>

                <div class="card-body">
                    <form method="POST" action=" {{ route('sendLeaveRequest') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <div class="alert alert-danger" role="alert">
                                Leave Balance : {{Auth::user()->leave_balance;}} days
                              </div>
                            <br>
                            @if (isset($alert))
                            @if ($alert=='no')
                            <div class="alert alert-success" role="alert"> request sent successfully </div>
                            @else
                            <div class="alert alert-danger" role="alert">  Your balance is not enough to take a leave </div>
                            @endif
                            @endif
                        <label for="exampleInputEmail1" class="form-label">Admin</label>
                        <select class="form-select" aria-label="Default select example" name="admin">
                            @foreach ($admins as $admin)
                            <option value="{{$admin->id}}">{{$admin->first_name." ".$admin->last_name}}</option>
                        @endforeach
                          </select>
                        </div>
                        <div class="mb-3">
                          <label class="form-label">beginning</label>
                          <input type="date" class="form-control" name="beginning" >
                        </div>
                          <div class="mb-3">
                          <label class="form-label">end</label>
                          <input type="date" class="form-control" name="end" >
                          </div>
                          <div class="mb-3">
                          <label class="form-label">status</label>
                          <input type="text" class="form-control" name="status" >
                          </div>
                          <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('send') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
