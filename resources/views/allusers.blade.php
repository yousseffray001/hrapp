@extends('layouts.app')

@section('content')
<div class="container">

                <table class="table container">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">email</th>
                        <th scope="col">gender</th>
                        <th scope="col">date of birth</th>
                        <th scope="col">position</th>
                        <th scope="col">delete</th>
                        <th scope="col">update</th>




                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->gender }}</td>
                            <td>{{ $user->date_of_birth }}</td>
                            <td>{{ $user->position }}</td>
                            <td><a href="{{route('delete',$user->id)}}"> <button type="button" class="btn btn-danger">delete</button></a></td>
                            <td><a href="{{route('update',$user->id)}}"> <button type="button" class="btn btn-success">update</button></a></td>
                        </tr>
                      @endforeach

                    </tbody>
                  </table>



</div>
@endsection
