
    @extends('layouts.app')
    @section('content')
    <div class="container">
    @if (count($leaves)==0)
    <div class="alert alert-info" role="alert">
      You do not have any leave request
    </div>
    @else

    <table class="table">
        <thead>
          <tr>
            <th scope="col">from</th>
            <th scope="col">Beginnig</th>
            <th scope="col">End</th>
            <th scope="col">Status</th>
            <th scope="col">Accept</th>
            <th scope="col">Refuse</th>
          </tr>
        </thead>

            @foreach ($leaves as $leave)

            <tr>
                <td>{{$leave->first_name." ".$leave->last_name}}</td>
                <td>{{$leave->beginning}}</td>
                <td>{{$leave->end}}</td>
                <td>{{$leave->status}}</td>
                <td><a href="{{route('acceptRequest',$leave->id)}}"><img src="{{asset('images/accept.jpg') }} " alt="accept image" width="40" style=" border-radius: 50%;" ></td>
                <td><a href="{{route('refuseRequest',$leave->id)}}"> <img src="{{asset('images/refuse.jpg')}}" alt="refuse image" width="40" style=" border-radius: 50%;" ></td>
            </tr>

            @endforeach

    </table>
    </div>
    @endif
    @endsection

