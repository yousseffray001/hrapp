<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::view('/profile', 'profile');


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
//admin routes
Route::get('/home/delete/{id}','UserController@delete')->name('delete')->middleware('auth');
Route::get('/home/update/{id}','UserController@redirectUpdate')->name('update')->middleware('auth');
Route::post('/home/updateUser/{id}','UserController@updateUser')->name('updateUser')->middleware('auth');
Route::get('/allusers','ShowUsersController@index')->name('allusers')->middleware('auth');
///
Route::get('/home/leaverequest','LeavesController@index')->name('leaverequest');
Route::post('/home/sendLeaveRequest','LeavesController@sendLeaveRequest')->name('sendLeaveRequest')->middleware('auth');
Route::get('/home/myLeaveRequests','LeavesController@myLeaveRequests')->name('myLeaveRequests')->middleware('auth');
Route::get('/home/allLeaveRequests','LeavesController@allLeaveRequests')->name('allLeaveRequests')->middleware('auth');
Route::get('/home/acceptRequest/{id}','LeavesController@acceptRequest')->name('acceptRequest')->middleware('auth');
Route::get('/home/refuseRequest/{id}','LeavesController@refuseRequest')->name('refuseRequest')->middleware('auth');
Route::get('/home/addcontract','AddContractController@index')->name('addcontract')->middleware('auth');
Route::post('/home/insertContract','AddContractController@insertContract')->name('insertContract')->middleware('auth');
