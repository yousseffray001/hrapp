<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public static function checkRole()
{
    if (Auth::user()->role!='admin')
    {
      echo "Sorry, You Are Not Allowed to Access This Page <a class='nav-link active' href='http://127.0.0.1:8000/home'>return to home page </a>";
      exit;
    }
}





}
