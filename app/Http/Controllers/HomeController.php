<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Leave;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $i=0;

        $users = DB::table('users')
        ->select('id', 'first_name','last_name')
        ->get();
        foreach ($users as $value) {
            $all[$i]= DB::table('leave')->where('user_id',$value->id)->count();
            $i++;
          }
        $i=0;
        foreach ($users as $value) {
        $accepted[$i]= DB::table('leave')->where('user_id',$value->id)->where('request_status','accepted')->count();
        $i++;
                }

        $i=0;
        foreach ($users as $value) {
         $refused[$i]= DB::table('leave')->where('user_id',$value->id)->where('request_status','refused')->count();
        $i++;
                        }

        return view('home',compact('all'),compact('refused'))->with('accepted',$accepted)->with('users',$users);


    }
}
