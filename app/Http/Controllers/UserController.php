<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth;
class UserController extends Controller
{



    public function delete($id)
    {
        UserController::checkRole();

        DB::delete('delete from users where id = ?',[$id]);
        return redirect()->route('allusers');
    }


    public function redirectUpdate($id)
    {
        UserController::checkRole();
        $user = DB::table('users')->where('id', $id)->first();


        return view('update')->with('user',$user);
    }

    public function updateUser(Request $request,$id)
    {
        UserController::checkRole();
        //guessExtension()
        //getMimeType()
        //store(
        //asStore()
        //storePublicly()
        //move()
        //getClientOriginalName()
        //getClientMimeType()
        //getClientExtension()
        //getSize()
        //getError()
        //isValid()
        //$request->file(image)->
        //$request->image->getSize()

        $request->validate([
'image'=>'required|mimes:jpg,png,jpeg|max:5048'
        ]);

$newImageName= time().'-'. $request->first_name . '.' . $request->image->extension();

$test=$request->image->move(public_path('images'),$newImageName);

       DB::table('users')
       ->where('id', $id)
       ->update([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
        'gender' => $request->gender,
        'date_of_birth' => $request->date_of_birth,
        'position' => $request->position,
        'leave_balance' => $request->leave_balance,
        'photo' => $newImageName,
        'password' => Hash::make($request->password),
    ]);

        return redirect()->route('allusers');
    }






}
