<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Contract;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
class AddContractController extends Controller


{
public function index ()
{
    UserController::checkRole();
    $users = DB::table('users')
    ->select('id', 'first_name','last_name')
    ->get();
  return view('addContract',compact('users'));

}

public function insertContract(Request $request)
{
//employee beginning type salary
DB::table('contracts')->insert([
    'user_id' => $request->employee,
    'beginning' => $request->beginning,
    'type' => $request->type,
    'salary' => $request->salary
]);
$users = DB::table('users')
->select('id', 'first_name','last_name')
->get();
return view('addContract',compact('users'))->with('alert','1');
}

}

