<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Leave;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Mail\SendAcceptMail;
use App\Mail\SendRefuseMail;
class LeavesController extends Controller
{
    public function index()
    {
        $admins =  DB::table('users')->where('role', '=', 'admin')->get();

        return view('sendLeaveRequest',compact('admins'));
    }

    public function sendLeaveRequest(Request $request)
    {
        $admins =  DB::table('users')->where('role', '=', 'admin')->get();
        $leave_balance = DB::table('users')->select('leave_balance')->where('id', '=', Auth::id())->get()->first();
        $leave_balance=$leave_balance->leave_balance;
        $datediff= strtotime($request->end)- strtotime($request->beginning)   ;
        $allDay=$datediff / (60 * 60 * 24);
if($allDay<=$leave_balance)
{
        DB::table('leave')->insert([
            'user_id' => Auth::id(),
            'admin_id' => $request->admin ,
            'beginning' => $request->beginning ,
            'end' => $request->end ,
            'status' => $request->status ,
            'request_status' => 'pending' ,
        ]);

        return view('sendLeaveRequest',compact('admins'))->with('alert','no');
    }
    else{

        return view('sendLeaveRequest',compact('admins'))->with('alert','yes');
    }
    }
    public function  myLeaveRequests()
    {
       $leaves =  DB::table('leave')
       ->join('users', 'leave.admin_id', '=', 'users.id') ->where('leave.user_id', '=', Auth::id())->get();
        return view('myLeaveRequests',compact('leaves'));

    }

    public function  allLeaveRequests()
    {
        UserController::checkRole();
        $leaves =  DB::table('leave')
        ->join('users', 'leave.user_id', '=', 'users.id')
         ->where('leave.admin_id', '=', Auth::id())
         ->where('leave.request_status', '=', 'pending')
         ->select('users.first_name', 'users.last_name', 'leave.*')
         ->get();

       return view('allLeaveRequests',compact('leaves'));

    }
    public function  refuseRequest($id)
    {
        UserController::checkRole();

         DB::table('leave')
         ->where('id', $id)
         ->update([
          'request_status' => 'refused',
        ]);
        $leaves =  DB::table('leave')
        ->join('users', 'leave.user_id', '=', 'users.id')
         ->where('leave.admin_id', '=', Auth::id())
         ->where('leave.request_status', '=', 'pending')
         ->select('users.first_name', 'users.last_name', 'leave.*')
         ->get();
         Mail::to(Auth::user()->email)->send(new SendRefuseMail);
       return view('allLeaveRequests',compact('leaves'));

    }
    public function  acceptRequest($id)
    {
//select beginning and end of request id
$x = DB::table('leave')->select('user_id','beginning','end')->where('id', '=',$id)->get()->first();
$datediff= strtotime($x->end)- strtotime($x->beginning)   ;
$allDay=$datediff / (60 * 60 * 24);
$leave_balance=DB::table('users')->select('leave_balance')->where('id', '=',$x->user_id)->get()->first();
//update leave balance in users table
DB::table('users')
->where('id', $x->user_id)
->update([
 'leave_balance' => $leave_balance->leave_balance-$allDay,
]);
         DB::table('leave')
         ->where('id', $id)
         ->update([
          'request_status' => 'accepted',
        ]);
        $leaves =  DB::table('leave')
        ->join('users', 'leave.user_id', '=', 'users.id')
         ->where('leave.admin_id', '=', Auth::id())
         ->where('leave.request_status', '=', 'pending')
         ->select('users.first_name', 'users.last_name', 'leave.*')
         ->get();


Mail::to(Auth::user()->email)->send(new SendAcceptMail);
       return view('allLeaveRequests',compact('leaves'));

    }




}

