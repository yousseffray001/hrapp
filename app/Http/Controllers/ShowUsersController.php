<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
class ShowUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        UserController::checkRole();
        //show all users

      $users=User::latest()->paginate(4);
      return view('allusers',compact('users'));
    }


}
