<?php

namespace App\Console\Commands;
use App\Models\User;
use App\Models\Leave;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class LeaveBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:leavebalance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add two points to the leave balance monthly';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = DB::table('users')
        ->select('id', 'leave_balance')
        ->get();
foreach ($users as $user) {
    DB::table('users')
    ->where('id', $user->id)
    ->update(['leave_balance' => $user->leave_balance+2]);

}
    }
}
